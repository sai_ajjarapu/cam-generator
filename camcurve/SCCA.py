# from cam import Cam

import numpy as np
from numpy import pi
from numpy import cos, sin
import matplotlib.pyplot as plt


def _calc_Ca(b, d):
    return 4 * pi ** 2 / ((pi ** 2 - 8) * (b ** 2 - d ** 2) - 2 * pi * (pi - 2) * b + pi ** 2)


class SCCA:
    """
    Class to calculate the theta, S, V, A, J for the SCCA curves
    """

    # CONSTANTS FOR EACH CURVE
    _CONSTANTS = {
        'accel': {'b': 0, 'c': 1, 'd': 0, 'Ca': _calc_Ca(0, 0)},
        'mod_trap': {'b': 0.25, 'c': 0.5, 'd': 0.25, 'Ca': _calc_Ca(0.25, 0.25)},
        'harm_disp': {'b': 0, 'c': 0, 'd': 1, 'Ca': _calc_Ca(0, 1)},
        'mod_sin': {'b': 0.25, 'c': 0, 'd': 0.75, 'Ca': _calc_Ca(0.25, 0.75)},
        'cyc_disp': {'b': 0.5, 'c': 0, 'd': 0.5, 'Ca': _calc_Ca(0.5, 0.5)}
    }

    def __init__(self, choice, start_beta, end_beta, h, num=100):
        assert choice in self._CONSTANTS.keys()
        self.choice = choice
        self.b = self._CONSTANTS[self.choice]['b']
        self.c = self._CONSTANTS[self.choice]['c']
        self.d = self._CONSTANTS[self.choice]['d']
        self.Ca = self._CONSTANTS[self.choice]['Ca']
        self.start_beta = start_beta
        self.end_beta = end_beta
        self.h = h
        self.num = num

        self.x = np.empty((5, num))
        self.t = np.empty((5, num))
        self.y = np.empty((5, num))
        self.s = np.empty((5, num))
        self.v = np.empty((5, num))
        self.a = np.empty((5, num))
        self.j = np.empty((5, num))

        self._theta()

        # TODO: Fix all these functions
        # self._displacement()
        # self._velocity()
        # self._acceleration()
        # self._jerk()
        # self._final_array()

    # TODO: add_rise
    def add_rise(self):
        pass

    # TODO: add_dwell
    def add_dwell(self):
        pass

    # TODO: add_fall
    def add_fall(self):
        pass

    def _theta(self):
        """
        FUNCTION TO CALCULATE THE NON-DIMENSIONALIZED ANGLE FOR EACH ZONE OF
        THE SCCA CURVE
        
        Paramaters
        ----------
        num : int, optional
            Number of linearly spaced datapoints for x and theta. Default is
            100. Use a larger number for better resolution.
        
        Returns
        -------
        """

        # Notes: Design of Machinery 5th Edition, Ch. 8, pg. 421-432
        # 
        #   - Zone 1: (0, b/2), b != 0
        #   - Zone 2: (b/2, (1-d)/2)
        #   - Zone 3: ((1-d)/2, (1+d)/2), d != 0
        #   - Zone 4: ((1+d)/2, 1 - b/2)
        #   - Zone 5: (1 - b/2, 1), b != 0
        #   - Zone 6: x > 1
        #
        # Make sure to handle overlapping datapoints

        # Declare local variables for convenience.
        b = self.b
        d = self.d
        num = self.num  # TODO ==> Move this to **kwargs

        self.x[0, :] = np.linspace(0, b / 2, num=num)  # x for zone 1
        self.x[1, :] = np.linspace(b / 2, (1 - d) / 2, num=num)  # x for zone 2
        self.x[2, :] = np.linspace((1 - d) / 2, (1 + d) / 2, num=num)  # x for zone 3
        self.x[3, :] = np.linspace((1 + d) / 2, 1 - b / 2, num=num)  # x for zone 4
        self.x[4, :] = np.linspace(1 - b / 2, 1, num=num)  # x for zone 5

        x = (self.x[0, :], self.x[1, :], self.x[2, :], self.x[3, :], self.x[4, :])
        self.X = np.concatenate(x, axis=0)

        # for i, arr in enumerate(x):
        #     setattr(self, f't{i+1}', arr)
        # self.theta = np.concatenate((self.t1, self.t2, self.t3, self.t4, self.t5))
        # self.theta *= self.end_beta - self.start_beta

    def _displacement(self):
        """
        FUNCTION TO CALCULATE THE NON-DIMENSIONALIZED DISPLACEMENT FOR EACH
        ZONE OF THE SCCA CURVE.
        """

        # TODO: Fix np.inf because they need to be a vector, not a single value.

        # Declare local variables just for convenience.
        b = self.b, d = self.d, c = self.c

        # Zone 1
        if b == 0:
            x = self.x[0, :]
            self.y1 = np.inf * np.ones(x.shape)
        else:
            x = self.x[0, :]
            self.y1 = b / pi * x - (b / pi) ** 2 * sin(pi / b * x)
            self.y1 *= self.Ca

        # Zone 2
        x = self.x[1, :]
        self.y2 = 0.5 * x ** 2 + b * (1 / pi - 0.5) * x + b ** 2 * (0.125 - 1 / pi ** 2)
        self.y2 *= self.Ca

        # Zone 3
        if d == 0:
            x = self.x[2, :]
            self.y3 = np.inf * np.ones(x.shape)
        else:
            x = self.x[2, :]
            self.y3 = (b / pi + c / 2) * x + (d / pi) ** 2 + b ** 2 * (1 / 8 - 1 / pi ** 2) - (1 - d) ** 2 / 8 \
                      - (d / pi) ** 2 * cos(pi / d * (x - (1 - d) / 2))
            self.y3 *= self.Ca

        # Zone 4
        x = self.x[3, :]
        self.y4 = -x ** 2 / 2 + (b / pi + 1 - b / 2) * x + (2 * d ** 2 - b ** 2) * (1 / pi ** 2 - 1 / 8) - 1 / 4
        self.y4 *= self.Ca

        # Zone 5
        if b == 0:
            x = self.x[4, :]
            self.x5 = np.inf * np.ones(x.shape)
        else:
            x = self.x[4, :]
            self.y5 = b / pi * x + 2 * (d ** 2 - b ** 2) / pi ** 2 + ((1 - b) ** 2 - d ** 2) / 4 - (b / pi) ** 2 \
                      * sin(pi / b * (x - 1))
            self.y5 *= self.Ca

        y = (self.y1, self.y2, self.y3, self.y4, self.y5)
        self.Y = np.concatenate(y, axis=0)

        # for i, arr in enumerate(y):
        #     setattr(self, f's{i+1}', self.h * arr)
        # self.S = np.concatenate((self.s1, self.s2, self.s3, self.s4, self.s5), axis=0)

    def _velocity(self):
        """
        Calculate the non-dimensionalized velocity for each of the 5 zones of
        the SCCA curve.
        """

        # TODO: Fix np.NaN because they need to be a vector, not a single value.

        # Declare local variables just for convenience.
        b = self.b, d = self.d, c = self.c

        # Zone 1
        try:
            x = self.x[0, :]
            self.v1 = b / pi * (1 - cos(pi / b * x))
        except ZeroDivisionError:
            x = self.x[0, :]
            self.v1 = np.inf * np.ones(x.shape)

        # Zone 2
        x = self.x[1, :]
        self.v2 = x + b * (1 / pi - 0.5)

        # Zone 3
        try:
            x = self.x[2, :]
            self.v3 = b / pi + c / 2 + d / pi * sin(pi / d * (x - (1 - d) / 2))
            # self.v3 = (b/pi + c/2)*x + ((self.d / pi) * sin((pi / self.d) * (self.x3 - ((1 - self.d) / 2))))
        except ZeroDivisionError:
            x = self.x[2, :]
            self.v3 = np.inf * np.ones(x.shape)

        # Zone 4
        x = self.x[3, :]
        self.v4 = -x + b / pi + 1 - b / 2

        # Zone 5
        try:
            x = self.x[4, :]
            self.v5 = b / pi * (1 - cos(pi / b * (x - 1)))
            # self.v5 = b/pi - ((self.b / pi) * cos((pi / self.b) * (self.x5 - 1)))
        except ZeroDivisionError:
            x = self.x[4, :]
            self.v5 = np.inf * np.ones(x.shape)

        self.V = np.concatenate((self.v1, self.v2, self.v3, self.v4, self.v5), axis=0)
        self.V *= self.Ca

    def _acceleration(self):
        """
        FUNCTION TO CALCULATE THE NON-DIMENSIONALIZED ACCELERATION FOR EACH ZONE OF THE SCCA CURVE
        :return: None
        """

        # Declare local variables just for convenience.
        b = self.b, d = self.d, c = self.c

        # Zone 1
        try:
            x = self.x[0, :]
            self.a1 = sin(pi / b * x)
        except ZeroDivisionError:
            x = self.x[0, :]
            self.a1 = np.inf * np.ones(x.shape)

        # Zone 2
        x = self.x[1, :]
        self.a2 = np.ones(x.shape)

        # Zone 3
        try:
            x = self.x[2, :]
            self.a3 = cos(pi / d * (x - (1 - d) / 2))
        except ZeroDivisionError:
            self.a3 = np.inf * np.ones(x.shape)

        # Zone 4
        x = self.x[3, :]
        self.a4 = -1 * np.ones(x.shape)

        # Zone 5
        try:
            x = self.x[4, :]
            self.a5 = sin(pi / b * (x - 1))
        except ZeroDivisionError:
            x = self.x[4, :]
            self.a5 = np.inf * np.ones(x.shape)

        self.A = np.concatenate((self.a1, self.a2, self.a3, self.a4, self.a5), axis=0)
        self.A *= self.Ca

    def _jerk(self):
        """
        FUNCTION TO CALCULATE THE NON-DIMENSIONALIZED JERK FOR EACH ZONE OF THE SCCA CURVE
        :return:
        """

        # Declare local variables just for convenience.
        b = self.b, d = self.d, c = self.c

        # Zone 1
        try:
            x = self.x[0, :]
            self.j1 = self.Ca * pi / b * cos(pi / b * x)
        except ZeroDivisionError:
            x = self.x[0, :]
            self.j1 = np.inf * np.ones(x.shape)

        # Zone 2
        x = self.x[1, :]
        self.j2 = np.zeros(x.shape)

        # Zone 3
        try:
            x = self.x[2, :]
            self.j3 = -self.Ca * pi / d * sin(pi / d * (x - (1 - d) / 2))
        except ZeroDivisionError:
            x = self.x[3, :]
            self.j3 = np.inf * np.ones(x.shape)

        # Zone 4
        x = self.x[3, :]
        self.j4 = np.zeros(x.shape)

        # Zone 5
        try:
            x = self.x[4, :]
            self.j5 = self.Ca * pi / b * cos(pi / b * (x - 1))
        except ZeroDivisionError:
            x = self.x[4, :]
            self.j5 = np.inf * np.ones(x.shape)

        self.J = np.concatenate((self.j1, self.j2, self.j3, self.j4, self.j5), axis=0)
        self.J *= self.Ca

    def _final_array(self):
        """
        FUNCTION TO GATHER ALL THE CALCULATED VALUED FOR EACH ZONE AND SORT BASED ON USER CHOICE INPUT AND
        RETURN THE FINAL DATA ARRAY FOR ALL FIVE ZONES IN ONE ARRAY
        OPTIONS:
        "accel" FOR ACCELERATION CURVE
        "mod_sine" FOR MODIFIED SINE CURVE
        "mod_trap" FOR MODIFIED TRAPEZOID CURVE
        "harm_disp" FOR SIMPLE HARMONIC DISPLACEMENT CURVE
        "cyc_disp" FOR CYCLODIAL DISPLACEMENT CURVE

        :param start_beta:
        :param end_beta:
        :param h:
        :return: theta, s, v, a, j
        """
        beta = self.end_beta - self.start_beta

        self.t = beta * self.X
        self.S = self.h * self.Y
        self.V *= self.h
        self.A *= self.h
        self.J *= self.h

        return self.t, self.S, self.V, self.A, self.J


__all__ = ['SCCA']

if __name__ == '__main__':
    scca = SCCA('mod_sin', 0, 45, 5)

    fig = plt.figure()
    fig.suptitle(t="SCCA('mod_sin', 0, 45, 5)")

    plt.subplot(2, 2, 1, projection='polar')
    plt.plot(scca.theta * np.pi / 180, scca.S)
    plt.title('Cam Curve', pad=20)

    plt.subplot(2, 2, 2)
    plt.plot(scca.theta, scca.V)
    plt.title('Velocity')

    plt.subplot(2, 2, 3)
    plt.plot(scca.theta, scca.A)
    plt.title('Acceleration')

    plt.subplot(2, 2, 4)
    plt.plot(scca.theta, scca.J)
    plt.title('Jerk')

    plt.legend()
    plt.tight_layout(pad=2.5)
    plt.show()

    print(1)
