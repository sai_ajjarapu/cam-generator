import numpy as np
from numpy import pi


def _calc_Ca(b, d):
    return 4 * pi ** 2 / ((pi ** 2 - 8) * (b ** 2 - d ** 2) - 2 * pi * (pi - 2) * b + pi ** 2)


class CamSection:

    # CONSTANTS FOR EACH CURVE
    _CONSTANTS = {
        'accel': {'b': 0, 'c': 1, 'd': 0, 'Ca': _calc_Ca(0, 0)},
        'mod_trap': {'b': 0.25, 'c': 0.5, 'd': 0.25, 'Ca': _calc_Ca(0.25, 0.25)},
        'harm_disp': {'b': 0, 'c': 0, 'd': 1, 'Ca': _calc_Ca(0, 1)},
        'mod_sin': {'b': 0.25, 'c': 0, 'd': 0.75, 'Ca': _calc_Ca(0.25, 0.75)},
        'cyc_disp': {'b': 0.5, 'c': 0, 'd': 0.5, 'Ca': _calc_Ca(0.5, 0.5)}
    }

    def __init__(self, choice, start_beta, end_beta, h, num=100):
        assert choice in self._CONSTANTS.keys()
        self.choice = choice
        self.b = self._CONSTANTS[self.choice]['b']
        self.c = self._CONSTANTS[self.choice]['c']
        self.d = self._CONSTANTS[self.choice]['d']
        self.Ca = self._CONSTANTS[self.choice]['Ca']
        self.start_beta = start_beta
        self.end_beta = end_beta
        self.h = h
        self.num = num

        self.x = np.empty((5, num))
        self.t = np.empty((5, num))
        self.y = np.empty((5, num))
        self.s = np.empty((5, num))
        self.v = np.empty((5, num))
        self.a = np.empty((5, num))
        self.j = np.empty((5, num))

    def _theta(self):
        pass

    def _displacement(self):
        pass

    def _velocity(self):
        pass

    def _acceleration(self):
        pass

    def _jerk(self):
        pass
