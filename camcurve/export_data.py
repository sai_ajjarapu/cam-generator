# FOR TESTING PURPOSES ONLY
from POLY import final_poly_data
import pandas as pd

c1 = final_poly_data("poly_345", 45, 120, 2)
theta, S, V, A, J = c1.func_choice()

path = r'SCCA_python_output_data.xlsx'

df1 = pd.DataFrame({'S': S})

writer = pd.ExcelWriter(path, engine='xlsxwriter')
df1.to_excel(writer)

writer.save()
writer.close()

