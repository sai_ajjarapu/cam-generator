# Development Setup Tutorial

The goal of this tutorial is to help create a fast-paced development environment.

Before uploading Python packages to PyPi, the developer(s) should test their code as well as test that their package
imports correctly.

Start by navigating to the top-level directory of your package:

```bash
$ cd <path\to\package>
```

If you do not see a file named `setup.py`, then you are not in the right directory.
```bash
cam-generator
├── camcurve
├── dependencies.yml
├── development.md
└── setup.py
```

## Install the package in a development conda environment (recommended)

We are going to install the `camcurve` package into a Anaconda environment so we don't cause any unwanted side-effects 
in the base environment.

```bash
$ conda activate <my env>
```

Make sure you have the required python packages already installed.
```bash
$ pip install setuptool wheel twine
```

If you don't have an environment yet, you can create one.
```bash
$ conda env create -n <env name> -f dependencies.yml
```

## Install the package locally

```bash
$ pip install -e .
```

I assume you are familiar with `pip install`. The `-e` flag tells pip to create a symbolic link to the package folder.
The `.` option tells pip to create the symbolic link using the current directory.

## Test that everything is working

Start a python interpreter and import the package.
```bash
>>> import camcurve
```

If no errors are raised, the package is installed locally.

__Note__: These steps should only have to be completed one time.