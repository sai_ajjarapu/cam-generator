from setuptools import setup, find_packages


setup(
    name='camcurve',
    version='0.0.1',
    description='Create cam curves.',
    packages=find_packages()
)
